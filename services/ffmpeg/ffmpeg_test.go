package ffmpeg

import (
	"context"
	"image"
	"reflect"
	"testing"
)

func TestExec_GetResolution(t *testing.T) {
	type fields struct {
		ffmpegFile string
		Loglevel   Loglevel
	}
	type args struct {
		ctx   context.Context
		input string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    image.Point
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "normal",
			fields: fields{
				ffmpegFile: "C:\\Users\\kayleeo\\software\\ffmpeg-gpl\\ffmpeg.exe",
				Loglevel:   LogLevelInfo,
			},
			args: args{
				ctx:   context.Background(),
				input: "C:\\Users\\kayleeo\\Desktop\\test\\横屏2K.mp4",
			},
			want: image.Point{
				X: 1920,
				Y: 1080,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &Exec{
				ffmpegFile: tt.fields.ffmpegFile,
				Loglevel:   tt.fields.Loglevel,
			}
			got, err := e.GetResolution(tt.args.ctx, tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetResolution() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetResolution() got = %v, want %v", got, tt.want)
			}
		})
	}
}
