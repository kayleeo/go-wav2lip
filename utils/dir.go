package utils

import (
	"os"
	"path/filepath"
)

// CreateTempDir 创建临时目录
func CreateTempDir(dir string) (string, error) {
	_ = os.Mkdir(dir, 0655)
	tempDir, err := os.MkdirTemp(dir, "")
	if err != nil {
		return "", err
	}
	tempDir, err = filepath.Abs(tempDir)
	if err != nil {
		return "", err
	}
	return tempDir, nil
}

// JoinDir 拼接目录并保证目录存在
func JoinDir(elem ...string) string {
	path := filepath.Join(elem...)
	os.MkdirAll(path, 0755)
	return path
}
